<?php
	
	Class extension_elrte_editor extends Extension	{	

		private $_headersAdded = false;

		public function about() {
			return array(
				'name' => 'Text Formatter: elRTE editor',
				'version' => '0.1',
				'release-date' => '2011-04-08',
				'author' => array(
					'name'     => '<a href="mailto:dixon@bk.ru">Alexander Ukolov</a>'
				),
				'description' => 'Includes <a href="http://elrte.org/en/">elRTE editor</a> WYSYWIG editor with built in code highlighter.'
			);
		}
	
		public function getSubscribedDelegates(){
			return array(
				array(
					'page'		=>	'/backend/',
					  'delegate'	=>	'ModifyTextareaFieldPublishWidget',
					  'callback'	=>	'applyEditor'
				)
			);
		}
		
		public function applyEditor($context) {	

			if( $context['field']->get('formatter') == 'elrte' or $context['field']->get('formatter') != 'elrtehtmleditor' )	{
			
				if( !$this->_headersAdded )	{
					
					Symphony::Engine()->Page->addScriptToHead( URL.'/extensions/elrte_editor/lib/elrte/js/jquery-ui-1.8.13.custom.min.js', 200, false );
					Symphony::Engine()->Page->addStylesheetToHead( URL.'/extensions/elrte_editor/lib/elrte/css/smoothness/jquery-ui-1.8.13.custom.css', 'screen', 20);
					
					Symphony::Engine()->Page->addScriptToHead( URL.'/extensions/elrte_editor/lib/elrte/js/elrte.min.js', 200, false );
					Symphony::Engine()->Page->addStylesheetToHead( URL.'/extensions/elrte_editor/lib/elrte/css/elrte.min.css', 'screen', 30);
					Symphony::Engine()->Page->addScriptToHead( URL.'/extensions/elrte_editor/lib/elrte/js/i18n/elrte.en.js', 210, false );
					
					Symphony::Engine()->Page->addScriptToHead( URL.'/extensions/elrte_editor/lib/codemirror/js/codemirror.js', 230, false );
					Symphony::Engine()->Page->addScriptToHead( URL.'/extensions/elrte_editor/lib/elrte.codehightlight.js', 240, false );
					
					Symphony::Engine()->Page->addScriptToHead( URL.'/extensions/elrte_editor/assets/symphony.elrte.js', 220, false );
					Symphony::Engine()->Page->addStylesheetToHead( URL.'/extensions/elrte_editor/assets/symphony.elrte.css', 'screen', 40);

					$this->_headersAdded = true;
				}
			
			}
		}
		
	}
