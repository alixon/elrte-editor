if (typeof Symphony.ADMIN == "undefined") {
	Symphony.ADMIN = window.location.toString().match(/^(.+\/symphony)/)[1];
}

jQuery(function()	{
	
	// See if there are any elrte textareas:
	jQuery('label > textarea.elrte,label > textarea.elrtehtmltidy').each(function() {
		
		jQuery(this).parent('label').parent().append( jQuery(this) );
		
		jQuery(this).elrte({
			doctype  : '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
			cssClass : 'el-rte',
			height   : jQuery(this).height(),
			toolbar  : 'maxi',
			cssfiles : ['/extensions/elrte_editor/lib/elrte/css/elrte-inner.css']
		});
		
	});

});