<?php

	Class formatterelrte extends TextFormatter{
		
		function about(){
			return array(
				'name' => 'elRTE editor',
				'version' => '0.1',
				'release-date' => '2011-04-08',
				'author' => array(
					'name'     => '<a href="mailto:dixon@bk.ru">Alexander Ukolov</a>'
				),
				'description' => 'elRTE is an open-source WYSIWYG editor written in JavaScript using jQuery UI'
			);
		}
		
		function run($string) {
			return $string;
		}

	}