<?php

	Class formatterElrteHtmlTidy extends TextFormatter{
		
		const 
			CHARSET = 'utf8';
		
		function about(){
			return array(
				'name' => 'elRTE editor + HTML Tidy',
				'version' => '0.1',
				'release-date' => '2012-05-26',
				'author' => array(
					'name'     => '<a href="mailto:dixon@bk.ru">Alexander Ukolov</a>'
				),
				'description' => 'elRTE editor frentend and HTML Tidy backend formatter provides valid xHTML output'
			);
		}
		
		function run($string) {
			
			$string = preg_replace('/<([@#%\?].+?)>/s', '&lt;$1&gt;', $string);
			
			$options = array(
				'indent' => false,
				'output-xhtml' => true,
				'hide-comments' => true,
				'numeric-entities' => true,
				'output-bom' => false,
				'preserve-entities' => false,
				'tidy-mark' => false,
				'show-body-only' => true,
				'enclose-text' => true,
				'force-output' => true,
			);

			$tidy = new Tidy();
			$tidy->parseString($string, $options, self::CHARSET);
			
			return $tidy->value;
		}

	}